import pickle
import warnings

import numpy as np
import pandas as pd

warnings.filterwarnings('ignore', message="X does not have valid feature names", category=UserWarning)

disease_to_field = {
    'Paroxysmal Positional Vertigo': ['Otolaryngology (ENT)'],
    'AIDS': ['Infectious Disease', 'Immunology'],
    'Acne': ['Dermatology'],
    'Alcoholic Hepatitis': ['Gastroenterology', 'Hepatology'],
    'Allergy': ['Immunology', 'Allergy and Immunology'],
    'Arthritis': ['Rheumatology'],
    'Bronchial Asthma': ['Pulmonology'],
    'Cervical Spondylosis': ['Orthopedics', 'Neurology'],
    'Chickenpox': ['Infectious Disease', 'Pediatrics'],
    'Chronic Cholestasis': ['Gastroenterology', 'Hepatology'],
    'Common Cold': ['Infectious Disease'],
    'Dengue': ['Infectious Disease'],
    'Diabetes': ['Endocrinology'],
    'Dimorphic Hemorrhoids (Piles)': ['Proctology', 'Gastroenterology'],
    'Drug Reaction': ['Pharmacology', 'Internal Medicine'],
    'Fungal Infection': ['Dermatology', 'Infectious Disease'],
    'GERD': ['Gastroenterology'],
    'Gastroenteritis': ['Gastroenterology', 'Infectious Disease'],
    'Heart Attack': ['Cardiology'],
    'Hepatitis B': ['Gastroenterology', 'Hepatology'],
    'Hepatitis C': ['Gastroenterology', 'Hepatology'],
    'Hepatitis D': ['Gastroenterology', 'Hepatology'],
    'Hepatitis E': ['Gastroenterology', 'Hepatology'],
    'Hypertension': ['Cardiology', 'Nephrology'],
    'Hyperthyroidism': ['Endocrinology'],
    'Hypoglycemia': ['Endocrinology'],
    'Hypothyroidism': ['Endocrinology'],
    'Impetigo': ['Dermatology', 'Infectious Disease'],
    'Jaundice': ['Gastroenterology', 'Hepatology'],
    'Malaria': ['Infectious Disease'],
    'Migraine': ['Neurology'],
    'Osteoarthritis': ['Rheumatology', 'Orthopedics'],
    'Paralysis (Brain Hemorrhage)': ['Neurology'],
    'Peptic Ulcer Disease': ['Gastroenterology'],
    'Pneumonia': ['Pulmonology', 'Infectious Disease'],
    'Psoriasis': ['Dermatology', 'Rheumatology'],
    'Tuberculosis': ['Infectious Disease', 'Pulmonology'],
    'Typhoid': ['Infectious Disease'],
    'Urinary Tract Infection': ['Urology', 'Infectious Disease'],
    'Varicose Veins': ['Vascular Surgery', 'Dermatology'],
    'Hepatitis A': ['Gastroenterology', 'Hepatology']
}



class PredictionModel:

    def __init__(self, data_dict, models):

        self.data_dict = data_dict

        self.rf_model = models[0]
        self.nb_model = models[1]
        self.svm_model = models[2]


    def save_model(self, file_name):

        pickle_file = open(file_name, 'wb')

        pickle.dump(self, pickle_file)

        pickle_file.close()

    @staticmethod
    def load_model(file_name):

        pickle_file = open(file_name, 'rb')

        model = pickle.load(pickle_file)

        pickle_file.close()

        return model


    # Defining the Function
    # Input: string containing symptoms separated by commas
    # Output: Generated predictions by models
    def predictDisease(self, symptoms):

        symptoms = symptoms.split(",")
        for i in range(len(symptoms)):
            symptoms[i] = symptoms[i].strip()

        # creating input data for the models
        input_data = [0] * len(self.data_dict["symptom_index"])
        # print(self.data_dict["symptom_index"].keys())
        for symptom in symptoms:
            index = self.data_dict["symptom_index"][symptom]
            input_data[index] = 1

        # reshaping the input data and converting it
        # into suitable format for model predictions
        input_data = np.array(input_data).reshape(1, -1)

        # generating individual outputs
        rf_prediction = self.data_dict["predictions_classes"][self.rf_model.predict(input_data)[0]]
        nb_prediction = self.data_dict["predictions_classes"][self.nb_model.predict(input_data)[0]]
        svm_prediction = self.data_dict["predictions_classes"][self.svm_model.predict(input_data)[0]]

        # print()
        # print(self.data_dict["predictions_classes"])
        # making final prediction by taking mode of all predictions
        final_prediction = pd.DataFrame([rf_prediction, nb_prediction, svm_prediction])[0][0]
        predictions = {
            "rf_model_prediction": rf_prediction,
            "naive_bayes_prediction": nb_prediction,
            "svm_model_prediction": svm_prediction,
            "final_prediction": final_prediction
        }

        from collections import Counter

        print(f"{predictions = }")

        # Count the occurrences of each prediction
        prediction_counts = Counter(predictions.values())

        # Find the most common prediction
        most_common_prediction = prediction_counts.most_common(1)[0][0]

        return most_common_prediction

    def suggest_department(self, disease):

        return disease_to_field.get(disease, ["General OPD"])




# if __name__ == "__main__":
#
#     departments = set()
#
#     for key, values in disease_to_field.items():
#         for value in values + ["General OPD"]:
#             departments.add(value)
#
#     print(departments)





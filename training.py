#### Training

#from scipy.stats import mode
# import matplotlib.pyplot as plt
# import seaborn as sns
# from sklearn.preprocessing import LabelEncoder
# from sklearn.model_selection import train_test_split, cross_val_score
# from sklearn.svm import SVC
# from sklearn.naive_bayes import GaussianNB
# from sklearn.ensemble import RandomForestClassifier
# from sklearn.metrics import accuracy_score, confusion_matrix


# train_dataset = pd.read_csv("datasets/Training.csv").dropna(
#     axis=1)
#
# disease_counts = train_dataset["prognosis"].value_counts()
# temp_df = pd.DataFrame({
#     "Disease": disease_counts.index,
#     "Counts": disease_counts.values
# })
#
# encoder = LabelEncoder()
# train_dataset["prognosis"] = encoder.fit_transform(train_dataset["prognosis"])
#
# X = train_dataset.iloc[:, :-1]
# Y = train_dataset.iloc[:, -1]
# X_train, X_test, Y_train, Y_test = train_test_split(
#     X, Y, test_size=0.2, random_state=24)
#
#
# # Defining scoring metric for k-fold cross validation
# def cv_scoring(estimator, X, Y):
#     return accuracy_score(Y, estimator.predict(X))
#
#
# models = {
#     "SVC": SVC(),
#     "Gaussian NB": GaussianNB(),
#     "Random Forest": RandomForestClassifier(random_state=18)
# }
#
# for model_name in models:
#     model = models[model_name]
#     scores = cross_val_score(model, X, Y, cv=10,
#                              n_jobs=-1,
#                              scoring=cv_scoring)
#
#
# # Training and testing SVM Classifier
# svm_model = SVC()
# svm_model.fit(X_train, Y_train)
# preds = svm_model.predict(X_test)
#
#
# cf_matrix = confusion_matrix(Y_test, preds)
#
# # Training and testing Naive Bayes Classifier
# nb_model = GaussianNB()
# nb_model.fit(X_train, Y_train)
# preds = nb_model.predict(X_test)
#
# cf_matrix = confusion_matrix(Y_test, preds)
#
# # Training and testing Random Forest Classifier
# rf_model = RandomForestClassifier(random_state=18)
# rf_model.fit(X_train, Y_train)
# preds = rf_model.predict(X_test)
#
#
# cf_matrix = confusion_matrix(Y_test, preds)
#
# symptoms = X.columns.values
#
# # Creating a symptom index dictionary to encode the
# # input symptoms into numerical form
# symptom_index = {}
# for index, value in enumerate(symptoms):
#     symptom = " ".join([i.capitalize() for i in value.split("_")])
#     symptom_index[symptom] = index
#
# data_dict = {
#     "symptom_index": symptom_index,
#     "predictions_classes": encoder.classes_
# }


# Saving
# prediction_model = PredictionModel(data_dict, [rf_model, nb_model, svm_model])
# prediction_model.save_model("saved_model.pkl")
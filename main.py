# Importing libraries

from prediction_model import PredictionModel


# Loading
prediction_model = PredictionModel.load_model("saved_model.pkl")

# prediction = prediction_model.predictDisease("Chills, Joint Pain, Shivering, Cough")
# prediction = prediction_model.predictDisease("Itching, Skin Rash, Cough, Pus Filled Pimples")
predicted_disease = prediction_model.predictDisease("Abnormal Menstruation, Diarrhoea")
department = prediction_model.suggest_department(predicted_disease)


print(f"Prediction\n\tDisease: {predicted_disease}\n\tDepartment: {department}")